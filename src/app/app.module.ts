import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http'

import { AppComponent } from './app.component';
import { ListadoPaisesComponent } from './components/listado-paises/listado-paises.component';
import { TarjetaPaisComponent } from './components/tarjeta-pais/tarjeta-pais.component';

@NgModule({
  declarations: [
    AppComponent,
    ListadoPaisesComponent,
    TarjetaPaisComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
