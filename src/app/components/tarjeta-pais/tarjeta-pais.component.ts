import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tarjeta-pais',
  templateUrl: './tarjeta-pais.component.html',
  styleUrls: ['./tarjeta-pais.component.css']
})
export class TarjetaPaisComponent implements OnInit {

  @Input() pais: any = {nombre:'Argentina'};
  @Output() onPaisSelected:EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    setTimeout(()=>{this.enviarMensajeAlPadre()},3000)
  }

  enviarMensajeAlPadre(){
    this.onPaisSelected.emit(this.pais)
  }

}
