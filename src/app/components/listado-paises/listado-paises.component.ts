import { Component, OnInit, Input } from '@angular/core';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-listado-paises',
  templateUrl: './listado-paises.component.html',
  styleUrls: ['./listado-paises.component.css']
})
export class ListadoPaisesComponent implements OnInit {

  constructor(private paisService: PaisService) { }
  paises: any[] = [];

  ngOnInit() {
    this.paisService.findAll()
      .subscribe((datos: any) => {
        this.paises = datos;
        console.log('resultado: ', datos)
      })
  }

  recibirPais(mensaje){
    console.log("mensaje",mensaje)
  }

}
